import argparse
from peewee import *
from collections import Counter
from datetime import datetime
from functions import *
FUNCTION_MAP = {'gender_percentile' : gender_percentile,
                'average_male_age' : average_male_age,
                'average_female_age': average_female_age,
                'average_age': average_age,
                'most_common_city': most_common_city,
                'most_common_password': most_common_password,
                'users_between': users_between,
                'strongest_password': strongest_password }
parser = argparse.ArgumentParser()
parser.add_argument('command', choices = FUNCTION_MAP.keys())
parser.add_argument('-n',"--numb",type=int, action ='store')
parser.add_argument('-d1','--date1',type=str,action='store')
parser.add_argument('-d2','--date2',type=str,action='store')
args = parser.parse_args()

func = FUNCTION_MAP[args.command]
if args.numb == None and args.date1 == None:
    func()
elif args.date1 == None:
    func(args.numb)
else:
    func(args.date1,args.date2)
