import random
import os
from os import system
system("mode con cols=60 lines=40")
os.system("cls")
print("     ############### BLACKJACK #############\n\n\n")

class Player():
    def __init__(self,name,cards,suma,balance):
        self.name=name
        self.suma=suma
        self.balance=balance
        self.cards=cards

    def __str__ (self):
        return f"Gracz: {self.name}\nKarty: {self.cards}\nSaldo: {self.balance}\nOczka: {self.suma}"

    def add_card(self,card,value):
        self.cards.append(card)
        if value==11 and self.suma>=11:
            value-=10
        self.suma+=value

    def amIbroke(self):
        return self.balance>0

    def money(self,bet):
        self.balance+=bet

    def oczka(self):
        return self.suma

    def shufle(self):
        self.cards=[]
        self.suma=0

    def saldo(self):
        return self.balance

    def allcards(self):
        return self.cards

class Dealer():
    def __init__(self,name,cards,suma,balance):
        self.name=name
        self.suma=suma
        self.balance=balance
        self.cards=cards

    def __str__ (self):
        return f"Krupier: {self.name}\nKarty: {self.cards[0]+'  ######'}\nSaldo: {self.balance}"

    def add_card(self,card,value):
        self.cards.append(card)
        if value==11 and self.suma>=11:
            value-=10
        self.suma+=value

    def money(self,bet):
        self.balance+=bet

    def oczka(self):
        return self.suma

    def allcards(self):
        return self.cards

    def shufle(self):
        self.cards=[]
        self.suma=0

def shufle():
    shuf = {'As Pik':11,'As Karo':11,'As Kier':11,'As Trefl':11,
           'Krol Pik':10,'Krol Karo':10,'Krol Kier':10,'Krol Trefl':10,
           'Dama Pik':10,'Dama Karo':10,'Dama Kier':10,'Dama Trefl':10,
           'Walet Pik':10,'Walet Karo':10,'Walet Kier':10,'Walet Trefl':10,
           '10 Pik':10,'10 Karo':10,'10 Kier':10,'10 Trefl':10,
           '9 Pik':9,'9 Karo':9,'9 Kier':9,'9 Trefl':9,
           '8 Pik':8,'8 Karo':8,'8 Kier':8,'8 Trefl':8,
           '7 Pik':7,'7 Karo':7,'7 Kier':7,'7 Trefl':7,
           '6 Pik':6,'6 Karo':6,'6 Kier':6,'6 Trefl':6,
           '5 Pik':5,'5 Karo':5,'5 Kier':5,'5 Trefl':5,
           '4 Pik':4,'4 Karo':4,'4 Kier':4,'4 Trefl':4,
           '3 Pik':3,'3 Karo':3,'3 Kier':3,'3 Trefl':3,
           '2 Pik':2,'2 Karo':2,'2 Kier':2,'2 Trefl':2}
    return shuf

deck=shufle()

def rollcard():
    card=random.choice(list(deck.keys()))
    cv=deck[card]
    deck.pop(card)
    return [card,cv]

def betuj():
    while(True):
        try:
            bet=int(input('Wielkosc zakladu: (max '+str(gamer.saldo())+"): "))
            if bet>gamer.saldo() or bet<=0:
                print("Niedozwolony zaklad! ")
                continue
        except:
            print("Niedozwolony zaklad! ")
        else:
            break
    gamer.money(-bet)
    dealer.money(-bet)
    return bet

def setup():
    [card1,cv1]=rollcard()
    gamer.add_card(card1,cv1)
    [card2,cv2]=rollcard()
    dealer.add_card(card2,cv2)
    [card3,cv3]=rollcard()
    gamer.add_card(card3,cv3)
    [card4,cv4]=rollcard()
    dealer.add_card(card4,cv4)
name=input("Nazwa gracza: ")
gamer=Player(name,[],0,100)
dealer=Dealer("Adam",[],0,100)

setup()

condition=True
print("Saldo gracza: "+str(gamer.saldo()))

bet=betuj()

while(condition):
    print(gamer)
    print('\n------------------------------------------------------------\n')
    print(dealer)
    while(True):
        try:
            move=int(input('1: Dobierz karte  2: Nie dobieraj '))
            if move!=1 and move!=2:
                print("Wybierz 1 lub 2 : ")
                continue
        except:
            print("Wybierz 1 lub 2 :")
        else:
            break
    print('\n------------------------------------------------------------\n')

    if move==1:
        [next_card,next_value]=rollcard()
        gamer.add_card(next_card,next_value)
        os.system("cls")
        print("     ############### BLACKJACK #############\n\n\n")
        print("Gracz dobiera karte: "+next_card)

        if gamer.oczka()>21:
            print("\nGracz przegrywa!")
            dealer.money(2*bet)
            print(gamer)
            print('\n------------------------------------------------------------\n')
            print("Karty krupiera: "+str(dealer.allcards()))
            print("Oczka: "+str(dealer.oczka()))
            print('\n------------------------------------------------------------\n')
        else:
            continue
    else:
        while(True):

            print(str(dealer.allcards()))
            print("Oczka: "+str(dealer.oczka()))

            if dealer.oczka()>gamer.oczka() and dealer.oczka()<22:
                print("Gracz przegrywa!")
                dealer.money(2*bet)
                break

            if dealer.oczka()<17:
                [next_card,next_value]=rollcard()
                dealer.add_card(next_card,next_value)
                print("Krupier dobiera karte: ")
                input("dowolny klawisz zeby kontynuowac")
                print("Dobiera: "+next_card)
                print('\n------------------------------------------------------------\n')
                continue

            if dealer.oczka()>=17 and  dealer.oczka()<gamer.oczka():
                print("Gracz wygrywa!")
                gamer.money(2*bet)
                break

            if dealer.oczka()>21:
                print("Gracz wygrywa!")
                gamer.money(2*bet)
                break

            if dealer.oczka()==gamer.oczka() and len(dealer.allcards())==len(gamer.allcards()):
                print("REMIS")
                gamer.money(bet)
                dealer.money(bet)
                break

            elif dealer.oczka()==gamer.oczka() and len(dealer.allcards())>len(gamer.allcards()):
                print("Gracz wygrywa!")
                gamer.money(2*bet)
                break

            elif dealer.oczka()==gamer.oczka() and len(dealer.allcards())<len(gamer.allcards()):
                print("Gracz przegrywa!")
                dealer.money(2*bet)
                break

            break
    condition=gamer.amIbroke()
    if condition==False:
        break
    gamer.shufle()
    dealer.shufle()
    deck=shufle()

    quit=input('ENTER - nastepne rozdanie / q - wyjscie ')
    if quit=='q':
        if gamer.saldo()>100:
            result='Wygrales: '
        else:
            result="Przegrales: "
        print('\n------------------------------------------------------------\n')
        print(result+str(abs(gamer.saldo()-100))+'!')
        input("dowolny klawisz aby zamknac...")
        os.system("cls")
        exit(0)

    os.system("cls")
    print("     ############### BLACKJACK #############\n\n\n")
    print("Nowe rozdanie!")
    print("Saldo gracza: "+str(gamer.saldo()))
    bet=betuj()
    setup()

os.system("cls")
print("     ############### BLACKJACK #############\n\n\n")
print(gamer)
print('\n------------------------------------------------------------\n')
print("Karty krupiera: "+str(dealer.allcards()))
print("Oczka: "+str(dealer.oczka()))
print("\n\n\nGra skonczona, jestes BANKRUTEM !!!")
input("dowolny klawisz aby zamknac...")
os.system("cls")
