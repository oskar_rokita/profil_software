from peewee import *
database = SqliteDatabase('myDB.db')

class UnknownField(object):
    def __init__(self, *_, **__): pass

class BaseModel(Model):
    class Meta:
        database = database

class MyTable(BaseModel):
    cell = TextField(null=True)
    dob = TextField(null=True)
    email = TextField(null=True)
    gender = TextField(null=True)
    id = TextField(null=True)
    location = TextField(null=True)
    login = TextField(null=True)
    name = TextField(null=True)
    nat = BareField(null=True)
    phone = TextField(null=True)
    registered = TextField(null=True)
    days_to_birth = IntegerField(null=True)
    dob_date = DateField(null=True)
    class Meta:
        table_name = 'people'
        primary_key = False
