from datetime import datetime
from peewee import *
from collections import Counter
from peewee_model import *
def gender_percentile():
    m = MyTable.select().where(MyTable.gender == 'male').count()
    f = MyTable.select().where(MyTable.gender == 'female').count()
    mp = m/(m+f)
    fp = f/(m+f)
    print(str(mp*100)+'%of men , '+str(fp*100)+'% of women')

def average_male_age():
    s = 0
    m = MyTable.select().where(MyTable.gender == 'male').count()
    for person in MyTable.select().where(MyTable.gender=='male'):
        s+=(eval(person.dob)['age'])
    print(round(s/m,2),'years')

def average_female_age():
    s = 0
    f = MyTable.select().where(MyTable.gender == 'female').count()
    for person in MyTable.select().where(MyTable.gender=='female'):
        s+=(eval(person.dob)['age'])
    print(round(s/f,2),'years')

def average_age():
    s = 0
    p = MyTable.select().count()
    for person in MyTable.select():
        s+=(eval(person.dob)['age'])
    print(round(s/p,2),'years')

def most_common_city(n):
    l = []
    for p in MyTable.select():
        l.append(eval(p.location)['city'])
    c = Counter(line.rstrip()for line in l)
    print(c.most_common(n))

def most_common_password(n):
    l = []
    for p in MyTable.select():
        l.append(eval(p.login)['password'])
    c = Counter(line.rstrip()for line in l)
    print (c.most_common(n))

def users_between(d1,d2):
    query = (MyTable.select().where(MyTable.dob_date.between(d1,d2)))
    for p in query:
        print(str(eval(p.name)['first'])+' '+str(eval(p.name)['last']))

def strongest_password():
    special_char = '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~'
    best_score = (0,'')
    for p in MyTable.select():
        con1 = con2 =con3=con4= True
        score = 0
        pas = (eval(p.login)['password'])
        for c in pas:
            if c.islower() and con1:
                score += 1
                con1 = False
            if c.isupper() and con2:
                score += 2
                cond2 = False
            if c.isdigit() and con3:
                score +=1
                con3 = False
            if c in special_char and con4:
                score +=3
                con4 = False
        if len(pas)>=8:
            score+=5
        if score > best_score[0]:
            best_score = (score,pas)
    print('Points: ',best_score[0],'Password: ',best_score[1])

def calculate_dates(now, date): # func to calculate days to next birthday
    while(True):
        try:
            delta1 = datetime(now.year, date.month, date.day)
            delta2 = datetime(now.year+1, date.month, date.day)
            days = (max(delta1, delta2) - now).days
            if days > 365:
                days -= 365
        except:
            delta1 = datetime(now.year, date.month, date.day)
            delta2 = datetime(now.year+1, date.month, date.day-1) # in case like 1984-02-29 ...
            days = (max(delta1, delta2) - now).days
            if days >365:
                days -= 365
            break
        else:
            break
    return days+1
