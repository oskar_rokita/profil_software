import sqlite3
from datetime import datetime
from functions import calculate_dates
# Run parse_json2db.py first to create myDB.db
conn = sqlite3.connect('myDB.db')
c = conn.cursor()
# New table without picture column
c.execute("""CREATE TABLE people(
        cell text,
        dob text,
        email text,
        gender text,
        id text,
        location text,
        login text,
        name text,
        nat text,
        phone text,
        registered  text,
        days_to_birth integer,
        dob_date text
    )""")
print("NEW TABLE CREATED")

# Filling new table with data
c.execute("SELECT cell,dob,email,gender,id,location,login,name,nat,phone,registered,rowid,rowid FROM myTable")
d = c.fetchall()
c.executemany("INSERT INTO people VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",d)
print("NEW TABLE FILLED WITH DATA WITHOUT picture column")
# Deleting old one
c.execute("DROP TABLE myTable")
print("OLD TABLE DROPED")

# Clean cell and phones numbers and calculate days_to_birth
c.execute("SELECT rowid,cell,phone,dob FROM people")
numbers = c.fetchall()
now = datetime.now()
for i in range(0,1000):
    nbc = nbp = ''
    for _ in numbers[i][1]:
        if str(_) in '1234567890':
            nbc += str(_)
    for _ in numbers[i][2]:
        if str(_) in '1234567890':
            nbp += str(_)
    date = datetime.strptime(eval(numbers[i][3])['date'][:10],'%Y-%m-%d')
    days = calculate_dates(now,date)
    c.execute("UPDATE people SET dob_date = ? WHERE rowid = ?",(date,i+1))
    c.execute("UPDATE people SET days_to_birth = ? WHERE rowid = ?",(days,i+1))
    c.execute("UPDATE people SET cell = ? WHERE rowid = ?",(nbc,i+1))
    c.execute("UPDATE people SET phone = ? WHERE rowid = ?",(nbp,i+1))

print("CELLs AND PHONEs NUMBERS UPDATED!")
print("DAYS TO BIRTH CALCULATED AND UPDATED!")

conn.commit()
conn.close()
