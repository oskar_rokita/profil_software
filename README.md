# Profil_Software

Zadanie rekrutacyjne 2020


**1.Jak postawić projekt?**

- git clone https://gitlab.com/oskar_rokita/profil_software.git

- cd profil_software

- python *parse_json2db.py*
- python  *main.py*


**2.Lista komend**

Dostęp do komend umożliwia skrypt *features.py*:

- gender_percentile           - wyświetla procentowy udział płci użytkowników w zbiorze bazy

    (np. python features.py gender_percentile)

- average_male_age            - średni wiek mężczyzn

- average_female_age          - średni wiek kobiet

- average_age                 - średni wiek wszysktich użytkowników


- most_common_city -n (NUMB)   - wyświetla NUMB najczęściej występujących miast

    (np. python features.py most_common_city -n 5)

- most_common_password -n (NUMB) - wyświetla NUMB najpopularniejszych haseł

- users_between -d1 YYYY-MM-DD -d2 YYYY-MM-DD - wyśiwetla użytkowników urodzonych pomiędzy datą d1 i d2

    (np. python features.py users_between -d1 1994-01-21 -d2 1996-01-01)

- strongest_password          - wyświetla nabezpieczniejsze hasło wg. wzoru w wymaganiach projektu

**3. Blackjack**
- do repozytorium dołączam również grę *'blackjack'*, którą napisałem jakiś czas temu w języku Python. Jest ona bardzo podobna do gry 'Oczko' 
z zadania frontendowego.
